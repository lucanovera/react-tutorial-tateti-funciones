import React, { useState } from "react";

function Counter() {
  console.log("Se llamo a la funcion");
  // Declaración de una variable de estado que llamaremos "count"
  const [count, setCount] = useState(0);
  console.log("React me paso como valor de count", count);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}
export default Counter;
